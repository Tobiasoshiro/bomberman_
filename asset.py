import juego


class bomberman():

    def __init__(self):
        self.pos = [600, 1]
        self.speed = 5
        self.number = 0
        self.alive = 1
        self.hitbox = 65

    def move(self, step, cols):
        """Prompts bomber to move"""
        new_pos = []
        print(self)
        for i, aux in enumerate(self.pos):
            new_pos.append(aux + step[i] * self.speed * cols)
        return new_pos

    def get_pos(self):
        return self.pos

    def take_damage(self, dmg):
        self.alive = self.alive - dmg

    def set_pos(self, new_pos):
        """Confirms new pos"""

        self.pos = new_pos
        print(self.pos)

    def get_hitbox(self):
        return self.hitbox


class wall():
    """another bricks the dust"""
    def __init__(self):
        self.pos = [300, 300]
        self.hitbox = 60

    def get_pos(self):
        return self.pos

    def set_pos(self, new_pos):
        self.pos = new_pos

    def get_hitbox(self):
        return self.hitbox


class bomb():
    """Set us up the bomb"""
    def __init__(self):
        self.pos = None

