import vista
import mapa
import asset
import juego
import pygame
import copy

pygame.init()


class control:
    def __init__(self):
        self.size = (800, 700)
        self.player = asset.bomberman()
        self.is_playing = True
        #self.a_wall = asset.wall()
        self.the_wall = 3
        self.level = [[300, 300], [300, 200], [300, 400]]
        self.model = juego.game(self.player, self.the_wall, self.level)
        self.screen = vista.graphics(self.player, self.model.build_level())

        self.play()

    def play(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    raise SystemExit
                    # is_playing = False
                pygame.display.flip()
                keys = pygame.key.get_pressed()
                if keys[pygame.K_LEFT]:
                    dir = "left"
                    self.model.move_asset(dir, self.player)
                    self.screen.refresh()
                if keys[pygame.K_RIGHT]:
                    dir = "right"
                    self.model.move_asset(dir, self.player)
                    self.screen.refresh()
                if keys[pygame.K_UP]:
                    dir = "up"
                    self.model.move_asset(dir, self.player)
                    self.screen.refresh()
                if keys[pygame.K_DOWN]:
                    dir = "down"
                    self.model.move_asset(dir, self.player)
                    self.screen.refresh()

if __name__ == "__main__":
    controlador = control()

    """def move_player(self):
        'Detects key prompts'
        dir = pygame.key.get_pressed(keys)
        self.model.move_asset(dir, player)"""


#class eventQ:
