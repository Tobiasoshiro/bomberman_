import control
import asset
import copy

direct = {'left': [-1, 0], 'right': [1, 0], 'up': [0, -1], 'down': [0, 1]}


class game():

    def __init__(self, player, the_wall, level):
        self.is_playing = False
        self.is_paused = False
        self.size = (800, 700)
        self.walls = the_wall
        self.level = level
        # Falta lista de niveles.

    def stop(self):
        is_playing = False

    def build_level(self):
        a_wall = asset.wall()
        the_wall = []
        for i in range(self.walls):
            w = copy.deepcopy(a_wall)
            w.set_pos(self.level[i])
            the_wall.append(w)
            print("WALLS: ", w.get_pos())
        print("WALL LIST", the_wall)
        self.walls = the_wall
        return the_wall

    def move_asset(self, dir, asset):
        # move_player -
        path = direct[dir]
        print(path)
        print(asset.get_pos())
        self.check_colisions(asset.move(path, 1), asset, path)

    def check_colisions(self, new_pos, asset, path):
        # no colisions = 1, else =0
        pos_aux1 = 1
        hitbox = asset.get_hitbox()
        for i, aux in enumerate(new_pos):
            pos_aux1 = pos_aux1 * ((aux + hitbox) < self.size[i]) * (aux > 0)
            print(pos_aux1)

        col_aux = 1
        # Colisones con paredes
        # col_aux = 1
        wallbox = self.walls[0].get_hitbox()
        for w, wall in enumerate(self.walls):
            wall_pos = wall.get_pos()
            w_aux = [[int(wall_pos[0] - wallbox / 2), int(wall_pos[1] - wallbox / 2)],
                    [int(wall_pos[0] + wallbox / 2), int(wall_pos[1] + wallbox / 2)]]
            print(w_aux)

            hit1 = copy.deepcopy(w_aux[0])
            hit2 = copy.deepcopy(w_aux[1])
            print("wallpos: ", hit1, " ", hit2)

            c_aux1 = ((hit1[0] <= new_pos[0]) & (new_pos[0] <=
                                                hit1[0] + wallbox) &
                                                ((new_pos[1] >= hit1[1]) &
                                                (new_pos[1] < hit1[1] +
                                                wallbox)))
            c_aux2 = ((hit1[1] <= new_pos[1]) & (new_pos[1] <=
                                                hit1[1] + wallbox) &
                                                ((new_pos[0] >= hit1[0]) &
                                                (new_pos[0] < hit1[0] +
                                                wallbox)))
            c_aux3 = ((hit2[0] >= new_pos[0]) & (new_pos[0] >=
                                                hit2[0] - wallbox) &
                                                ((new_pos[1] <= hit2[1]) &
                                                (new_pos[1] > hit2[1] -
                                                wallbox)))
            c_aux4 = ((hit2[1] >= new_pos[1]) & (new_pos[1] >=
                                                hit2[1] - wallbox) &
                                                ((new_pos[0] <= hit2[0]) &
                                                (new_pos[0] > hit2[0] -
                                                wallbox)))
            c_aux = ((c_aux1 == 1) + (c_aux2 == 1) +
                    (c_aux3 == 1) + (c_aux4 == 1))
            print("colpos: ", c_aux, c_aux1, c_aux2, c_aux3, c_aux4)

            col_aux = col_aux * (c_aux == 0)

        print(col_aux)
        pos_aux2 = 1 * col_aux
        print(pos_aux2)

        colisions = pos_aux1 * pos_aux2  # faltan operaciones
        asset.set_pos(asset.move(path, colisions))
