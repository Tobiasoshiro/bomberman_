import pygame
import juego
import control


class main_map:
    def __init__(self, player):
        self.model = juego.game
        self.size = [700, 800]
        self.player = player

    def get_coordinates(self, asset):
        asset_pos = asset.get_pos()
        print("WALLS MAP ", asset_pos)
        return asset_pos

    def get_hitbox(self, asset):
        return asset.get_hitbox()
