import juego
import asset
import control
import pygame
import mapa
import os

assets = {'bomberman': 'killaqueen', 'NPC': 'doggo'}


class graphics(object):

    def __init__(self, player, walls):
        pygame.init()
        self.size = (800, 700)
        self.background = None
        self.model = juego.game
        self.screen = pygame.display.set_mode(self.size)
        self.bomber = None
        self.player = player
        self.map = mapa.main_map(self.player)
        self.link = os.getcwd()
        self.the_wall = walls
        self.the_bricks = None

        self.load_screen()
        self.load_player()
        self.load_walls()

        pygame.key.set_repeat(20)

    def load_screen(self):
        self.background = pygame.image.load('{}/dims.png'.format(self.link))
        self.screen.blit(self.background, [0, 0])
        pygame.display.update()

    def refresh(self):
        player_pos = self.map.get_coordinates(self.player)
        self.screen.blit(self.background, [0, 0])
        # self.bomber = pygame.image.load('{}killaqueen{}.png'.format(self.link))
        self.screen.blit(self.bomber, player_pos)
        for i, w in enumerate(self.the_wall):
            wall_pos = self.map.get_coordinates(w)
            print(wall_pos)
            self.screen.blit(self.the_bricks, wall_pos)
        pygame.display.update()
        # aaaaaaaaa

    def load_player(self):
        self.bomber = pygame.image.load('{}/killaqueen.png'.format(self.link))
        hitbox = self.map.get_hitbox(self.player)
        self.bomber = pygame.transform.scale(self.bomber, (hitbox, hitbox))
        player_pos = self.map.get_coordinates(self.player)
        print(player_pos)
        self.screen.blit(self.bomber, player_pos)
        pygame.display.update()

    def load_walls(self):
        self.the_bricks = pygame.image.load('{}/Standard_wall.png'.format(self.link))
        wallbox = self.map.get_hitbox(self.the_wall[0])
        self.the_bricks = pygame.transform.scale(self.the_bricks, (wallbox, wallbox))        
        for i, w in enumerate(self.the_wall):
            wall_pos = self.map.get_coordinates(w)
            print(wall_pos)
            self.screen.blit(self.the_bricks, wall_pos)
        pygame.display.update()
